package calculator;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Controller {

    private static final Logger log = LoggerFactory.getLogger(Controller.class);

    @FXML
    private Button button1;

    @FXML
    private TextField textField;

    @FXML
    private Button button2;

    @FXML
    void runButtonAction(ActionEvent event) {
        log.info("Wciśnięto: " + event.getSource());
        log.debug("wwww");
        log.error("ERROR");
        if (event.getSource() == button1) {
            textField.setText("Kliknięto b1");
        } else if (event.getSource() == button2) {
            textField.setText("Kliknięto b2");
        }
    }
}
